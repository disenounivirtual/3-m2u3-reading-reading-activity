const img = [
    {
        "id":1,
        "img": "./media/img1.png",
        "name": "A college student",
        "tooltip": "I’m Daniel and I’m a college student. University life is fun but it is difficult sometimes. I also work in a restaurant. A typical day for me starts early because I start classes at 7:00 a.m. I never eat breakfast at home, I only drink a cup of coffee. When I’m at university, I eat a hamburger and some soda for lunch. For dinner, I usually eat cookies, a sandwich and soda. I finish my day at 12:00 a.m. On weekends, I normally have lunch before work and I drink cold tea and cookies. When I finish work, I eat pizza or a hot dog. I don’t do much exercise because I don’t like it and I don’t have any free time."
    },
    {
        "id":2,
        "img": "./media/img2.png",
        "name": "A Math Teacher",
        "tooltip": "My name is Marilyn and I’m a Math teacher at university. Every morning I get up at 5:00 a.m. and I have a shower. Then, I get dressed and I prepare breakfast for me and my two teen children, Jennifer and Brandon. I normally prepare some fruit and cereal for breakfast. When I’m at home, I clean the house. Later, I go to the gym until 10:00 a.m. After that, I prepare lunch and go to university. I work in the afternoon and evenings. For lunch, I have a sandwich, a granola bar and orange juice. Finally, I arrive home at 10:00 p.m. I talk to my children about our day, prepare classes and I go to bed at 2:00 a.m."
    },
    {
        "id":3,
        "img": "./media/img3.png",
        "name": "A senior",
        "tooltip": "My name is Gerardo, I’m 67 years old and I’m retired. I wake up at 7:30 a.m. and I brush my teeth, wash my face and get dressed. I prepare breakfast and I eat. I usually have a cup of coffee, toast and boiled eggs. Later, I take a walk with my dog Bruno. I do exercise in my stationary bicycle. Later, I go shopping. I buy some food for the week and I eat at a restaurant in the city centre I usually have fish. Then, I arrive home at 2:30 p.m. and I take the dog for a walk again. At 5:30 p.m. I prepare dinner. I don’t feel very hungry at that time so I have a cheese sandwich and fruit juice. Then, I watch the news on TV and I go to sleep 9:00 p.m."
    },
]
export default img